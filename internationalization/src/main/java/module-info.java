/**
 * 
 */
/**
 * @author SVTI
 *
 */
open module internationalization {
	requires java.sql;
	requires java.instrument;

	requires spring.beans;
	requires spring.boot;
	requires spring.boot.autoconfigure;
	requires spring.context;
	requires spring.core;
	requires spring.web;
	requires spring.webmvc;
}